andywestken (4031)
With a map, operator[] will always return a value. If it's the first time you're used a key, the value will be the default.
http://www.cplusplus.com/reference/stl/map/operator%5B%5D/

You need to use find() to test for existence.
http://www.cplusplus.com/reference/stl/map/find/
	

map<pair<int,int>, double> mymap;

...

mymap[make_pair(x,y)] = specificValue;

...

// check for existence
if(mymap.find(make_pair(x,y)) != mymap.end())
    ...

map<pair<int,int>, double>::iterator iter = mymap.find(make_pair(x,y));
if(iter != mymap.end())
{
    double value = (*iter).second; // or iter->second;
    ...
}

	
